Rails.application.routes.draw do
  root 'admin/dashboard#index'
  # get 'user/index'
  # get 'user/show'
  # get 'user/update'
  # get 'user/delete'
  # get 'category/create'
  # get 'category/update'
  get 'categories' => 'category#index'
  get 'articles' => 'article#index'
  # get 'category/show'
  # get 'category/delete'
  devise_for :users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
