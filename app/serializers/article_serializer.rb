class ArticleSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :image

  belongs_to :user
  belongs_to :category
end
