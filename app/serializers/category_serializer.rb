class CategorySerializer < ActiveModel::Serializer
  attributes :id, :title

  belongs_to :parent
end
