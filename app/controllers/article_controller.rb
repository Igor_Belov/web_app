class ArticleController < ApplicationController
  def index
    @articles = Article.includes(:user, :category).all

    render json: @articles
  end
end
